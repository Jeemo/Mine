<?php
/**
 * @file
 * a_fresh_default_setup_basic.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function a_fresh_default_setup_basic_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: superuser.
  $roles['superuser'] = array(
    'name' => 'superuser',
    'weight' => 2,
  );

  return $roles;
}
